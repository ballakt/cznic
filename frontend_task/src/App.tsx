import './App.css';
import Articles from './dataset.json';
import { StickyContainer } from 'react-sticky';
import ArticlesComponent from './components/articles'
import Header from './components/header';
import { DispatchSearchEventType, FilterByType, SearchContext } from './context/search';
import { useState } from 'react';
import { Footer } from './components/footer';
import Loader from './components/loader';

function App() {

  const [query, setQuery] = useState<string>("")
  const [filterBy, setFilterBy] = useState<FilterByType> ('NAME')
  const [activeArticles, setActiveArticles] = useState<number> (Articles.articles.length)
  const [loading, setLoading] = useState<number>(0)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isRegexpActive, setIsRegexpActive] = useState<boolean>(false)

  const dispatchSearchEvent: DispatchSearchEventType = (actionType, payload) =>  {
    switch (actionType){
      case 'SET_QUERY': {
        if (payload.query !== undefined){
          setQuery(payload.query)
        }
        return;
      }
      case 'SET_FILTER': {
        if (payload.filterBy !== undefined){
          setFilterBy(payload.filterBy)
        }
        return
      }
      case 'SET_ACTIVE_ARTICLES': {
        if (payload.activeArticles !== undefined){
          setActiveArticles(payload.activeArticles)
        }
        return
      }
      case 'INC_LOADING': {
        setLoading(loading + 1)
        if (loading > 0){
          setIsLoading(true)
        }
        if (loading <= 0){
          setIsLoading(false)
        }
        return
      }
      case 'DEC_LOADING': {
        setLoading(loading -1)
        if (loading <= 0){
          setIsLoading(false)
        }
        if (loading > 0){
          setIsLoading(true)
        }
        return
      }
      case 'TOGGLE_REGEX': {
        setIsRegexpActive(!isRegexpActive)
        return
      }
      default:
        return;
    }
  }
  return (
    <div className="App">
      <SearchContext.Provider value={{ query, filterBy, activeArticles, dispatchSearchEvent, isLoading, isRegexpActive }}>
        <main className="container pt-4">
          <StickyContainer>
            <Header/>
            <Loader/>
            <ArticlesComponent/>
          </StickyContainer>
        </main>
        <Footer/>
      </SearchContext.Provider>
    </div>
  );
}

export default App;

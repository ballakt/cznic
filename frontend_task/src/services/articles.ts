import { INotConnectedArticle } from '../components/articles'
import { FilterByType } from '../context/search'
import Articles from '../dataset.json'

export type ComparatorType = (value: string, query: string) => boolean
export const defaultComparator: ComparatorType = (value, query) => {
    if (value === undefined){
        return false
    }
    return value.toLowerCase().includes(query)
}
export const regexComparator: ComparatorType = (value, query) => {
  try {
      return value.match(query) !== null
  } catch (error) {
      return false
  }
}

const findArticlesByName = (query: string, comparator:ComparatorType): Set<number> => {
    const article_ids = new Set<number>()
    const { articles } = Articles
    articles.forEach((article) => {
        if (comparator(article.name, query)){
        article_ids.add(article.id)
        }
    })
    return article_ids
}
const findArticlesByAuthor = (query: string, comparator:ComparatorType): Set<number> => {
  const article_ids = new Set<number>()
  const { authors } = Articles
  authors.forEach((author) => {
    if (comparator(author, query)){
      //@ts-ignore
      const foundIndex = (Articles.author_indexes[author] as number[])
      foundIndex.forEach((id: number) => {
        article_ids.add(id)
      });
    }
  })
  return article_ids
}
const findArticlesByCategory = (query: string, comparator:ComparatorType): Set<number> => {
  const article_ids = new Set<number>()
  const { categories } = Articles
  categories.forEach((category) => {
    if (comparator(category, query)){
      //@ts-ignore
      const foundIndex = (Articles.category_indexes[category] as number[])
      foundIndex.forEach((id: number) => {
        article_ids.add(id)
      });
    }
  })
  return article_ids
}
const findArticlesByAuthorAndName = (query: string, comparator:ComparatorType): Set<number> => {
  const byAuthor = findArticlesByAuthor(query, comparator)
  const byName = findArticlesByName(query, comparator)
  const intersectionResult = new Set<number>()

  byAuthor.forEach((articleId: number) => {
    if(byName.has(articleId)) {
      intersectionResult.add(articleId)
    }
  })
  return intersectionResult
}
const findArticlesByAuthorOrCategoryOrName = (query: string, comparator: ComparatorType): Set<number> => {
  const byAuthor = findArticlesByAuthor(query, comparator)
  const byName = findArticlesByName(query, comparator)
  const byCategory = findArticlesByCategory(query, comparator)

  return new Set<number>([...byAuthor, ...byName, ...byCategory])
}

const getArticleIDs = (filterBy: FilterByType, query: string, isQueryRegex: boolean) => {
  let article_ids = new Set<number>()
  const lowerCased = query.toLowerCase()
  const comparator = isQueryRegex ? regexComparator : defaultComparator
  switch (filterBy){
    case "ALL": 
          article_ids = findArticlesByAuthorOrCategoryOrName(lowerCased, comparator)
          break;
    case "AUTHOR": 
          article_ids = findArticlesByAuthor(lowerCased, comparator) 
          break
    case "AUTHOR_AND_NAME": 
          article_ids = findArticlesByAuthorAndName(lowerCased, comparator)
          break;
    case "CATEGORY": 
          article_ids = findArticlesByCategory(lowerCased, comparator)
          break
    case "NAME":
          article_ids = findArticlesByName(lowerCased, comparator)
          break;
  }
  return article_ids
}
export const getSortedAndFilteredArticles = (filterBy: FilterByType, query: string, isQueryRegexp: boolean) => {
  const article_ids = getArticleIDs(filterBy, query, isQueryRegexp)
  const articles = Articles.articles.filter((article: INotConnectedArticle) => article_ids.has(article.id))
  return isQueryRegexp ?  articles : getSortedArticles(filterBy, query, articles)
}
type SortingFunctionType = (article_1: INotConnectedArticle, article_2: INotConnectedArticle, query: string) => number
const sortingByStartsWithString = (string1: string, string2: string, compareTo: string) => {
  const string1Lowered = string1.toLowerCase()
  const string2Lowered = string2.toLowerCase()

  if (string1Lowered.startsWith(compareTo) && !string2Lowered.startsWith(compareTo)) {
    return 1
  }
  if (!string1Lowered.startsWith(compareTo) && string2Lowered.startsWith(compareTo)) {
    return -1
  }
  return 0
}
const stringDataStartsWith = (ID: number, query: string, data: string[]) => {
  if (data[ID] == undefined){
    return false
  }
  return data[ID-1].toLowerCase().startsWith(query)
}
const relatedStringDataStartsWith = (IDs: (number | null)[], query: string, data: string[]) => {
  for (const ID of IDs){
    if (ID == null){
      continue
    }
    if (stringDataStartsWith(ID, query, data)){
      return true
    }
  }
  return false
}
const sortingByRelatedStringDataStartsWith = (ids1: (number|null)[], ids2: (number|null)[], query: string, data: string[]) => {
  const result1 = relatedStringDataStartsWith(ids1, query, data)
  const result2 = relatedStringDataStartsWith(ids2, query, data)
  if (result1 && !result2){
    return 1
  }
  if (!result1 && result2){
    return -1
  }
  return 0
}
const sortingByName: SortingFunctionType = (article1, article2, query) => {
  return sortingByStartsWithString(article1.name, article2.name, query)
}
const sortingByAuthor: SortingFunctionType = (article1, article2, query) => {
  return sortingByRelatedStringDataStartsWith([article1.author], [article2.author], query, Articles.authors)
}
const sortingByCategory: SortingFunctionType = (article1, article2, query) => {
  return sortingByRelatedStringDataStartsWith(article1.categories, article2.categories, query, Articles.categories)
}

const sortArticles = (articles: INotConnectedArticle[], sortingBy: SortingFunctionType[], query: string) => {
  articles.sort((first: INotConnectedArticle, second: INotConnectedArticle) => {
    let sortVal: number = 0
    sortingBy.forEach( (sortF) => {
      sortVal = sortVal || -sortF(first, second, query)
    })
    return sortVal
  })
}
const getSortedArticles = (filterBy: FilterByType, query: string, articles: INotConnectedArticle[]) => {
  const lowerCased = query.toLowerCase()
  switch (filterBy) {
    case "ALL": 
      //prioritize order: name, author and category
      sortArticles(articles, [sortingByName,sortingByAuthor,sortingByCategory], lowerCased)
      break;
    case "AUTHOR":
      sortArticles(articles, [sortingByAuthor], lowerCased)
      break;
    case "AUTHOR_AND_NAME":
      sortArticles(articles, [sortingByName, sortingByAuthor], lowerCased)
      break;
    case "CATEGORY":
      sortArticles(articles, [sortingByCategory], lowerCased)
      break;
    case "NAME":
      sortArticles(articles, [sortingByName], lowerCased)
      break;
  }
  return articles
}
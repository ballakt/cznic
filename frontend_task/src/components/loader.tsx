import { useContext } from 'react';
import { Spinner } from 'react-bootstrap';
import { SearchContext } from '../context/search';

const Loader = () => {
    const { isLoading } = useContext(SearchContext)

    const hidden = isLoading ? "" : "d-none"
    return (<Spinner animation="border" role="status" className={hidden}>
        <span className="visually-hidden">Loading...</span>
    </Spinner>)
}
export default Loader
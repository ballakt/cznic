import { useContext, useEffect, useState, useCallback } from "react"
import { Card, Col, Row, Image } from "react-bootstrap"
import LinesEllipsis from "react-lines-ellipsis"
import { SearchContext } from "../context/search";
import Articles from '../dataset.json';
import { PreparedArticles, useDataset } from "../hooks/articles";
import { usePrevious } from "../hooks/utils";
import {getSortedAndFilteredArticles } from "../services/articles";
import Placeholder from "./placeholder";

export interface IArticleImage {
  source: string
  caption: string
}
export interface IArticle {
    id: number
    name: string
    author: string | null
    category: string[] 
    img: IArticleImage
}
type Indexes = Record<string,number[]>
type IDdictionary = Record<string,number>
export interface INotConnectedArticle {
  id: number
  name: string
  author: number | null
  categories: number[]
  img: IArticleImage
}

export interface IData {
  author_indexes: Indexes
  categories_indexes: Indexes
  category_ids: IDdictionary
  author_ids: IDdictionary
  authors: string[]
  categories: string[]
  articles: INotConnectedArticle[]
}
interface ArticleProps {
    article: IArticle
    key: number
}
const initialEllipsisValue = 5
const Article = (props: ArticleProps) => {
    const [ellipsisMaxLine, setEllipsisMaxLine] = useState<number>(initialEllipsisValue)
    const [isFocused, setIsFocused] = useState<boolean>(false)
    const maxOutEllipsis = () => setEllipsisMaxLine(14)
    const minimizeEllipsis = () => setEllipsisMaxLine(initialEllipsisValue)
    const { article } = props
    return <Col 
                lg={4} 
                key={props.key}
                className="text-start mt-4"
                tabIndex={-1}
                onFocus={() => {maxOutEllipsis(); setIsFocused(true)}}
                onBlur={() => {minimizeEllipsis(); setIsFocused(false)}}
                onMouseEnter={maxOutEllipsis}
                onMouseLeave={() => !isFocused && minimizeEllipsis()}>
      <article className="card" >
        <Row>
        <Col xs={8}>
            <Card.Body>
            <Card.Title className="h6">
              <LinesEllipsis
                  text={article.name}
                  maxLine={ellipsisMaxLine}
                  ellipsis='...' 
                  basedOn='letters'
                />
            </Card.Title>
            <Card.Text className="border-top pt-2">
            <div>
              <span className="fw-bold">Author:</span> 
              {" " + article.author}
            </div>
            <div>
              <span className="fw-bold">Category:</span>
              {" " + article.category}
            </div>
            </Card.Text>
            </Card.Body>
        </Col>
        <Col xs={4}>
            <Image src={article.img.source} alt={article.img.caption} className="img-fluid rounded-end"/>
        </Col>
        </Row>
      </article>
    </Col>
  }

const ArticlesComponent = () => {
  const [dataset, setDataset] = useDataset()
  const { query, filterBy, dispatchSearchEvent, isRegexpActive } = useContext(SearchContext)

  const previous = usePrevious(query)

  const loadingStart = () => {
    dispatchSearchEvent("INC_LOADING", {})
  }
  const loadingEnd = () => {
    dispatchSearchEvent("DEC_LOADING", {})
  }

  useEffect(() => {
    if (previous !== "" && query === ""){
      setDataset(Articles.articles)
      dispatchSearchEvent("SET_ACTIVE_ARTICLES", { activeArticles: Articles.articles.length })
      return
    }
    if (query === ""){
      return
    }
    loadingStart()
    console.log(isRegexpActive)
    const newArticles = getSortedAndFilteredArticles(filterBy, query, isRegexpActive)
    dispatchSearchEvent("SET_ACTIVE_ARTICLES", { activeArticles: newArticles.length })
    setDataset(newArticles)
    loadingEnd()
  }, [filterBy, query, isRegexpActive])

  return(<>{dataset.length == 0 ? <Placeholder/> : dataset.map((articles: PreparedArticles, index: any) => 
    <Row key={index}>
    {articles.map((article: INotConnectedArticle, index) => {             
      const finalAuthor: IArticle = {
        id: article.id,
        name: article.name,
        author: article.author ? Articles.authors[article.author - 1] : 'Unknown',
        category: article.categories.map(id => Articles.categories[id-1]),
        img: article.img
      }
      return <Article article={finalAuthor} key={index}/>
    })}
    </Row>
    )}</>)
  
}
export default ArticlesComponent
import { Container } from "react-bootstrap"

const Placeholder = () => {
    return <div className="floating-in-middle">
        <p className="fw-bolder fs-4 mt-5 ">No results</p>
    </div>
}
export default Placeholder
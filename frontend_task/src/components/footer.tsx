import { useContext } from 'react';
import { SearchContext } from '../context/search';
import Articles from '../dataset.json'
export const Footer = () => {
const { activeArticles } = useContext(SearchContext);

return (<footer className="shadow zindex-fixed d-flex align-items-center justify-content-center text-white shadow rounded-pill">
    <span className="fw-bold">{activeArticles}</span>/<span>{Articles.articles.length}</span>
</footer>)}
import { useContext } from "react"
import { Navbar, Container, Row, Col, FormControl, Form } from "react-bootstrap"
import { Sticky } from "react-sticky"
import { useSearch } from "../hooks/articles"
import { SearchContext } from "../context/search"

const Header = () => {
  const { dispatchSearchEvent, filterBy, isRegexpActive } = useContext(SearchContext);
  const [searchText, setSearchText] = useSearch((query: string) => dispatchSearchEvent("SET_QUERY",{query}))
  const handleSelector = (event: any) => {
    dispatchSearchEvent("SET_FILTER", { filterBy: event.target.value })
  }
  const handleRegexpCheckbox = (event: any) => {
    dispatchSearchEvent("TOGGLE_REGEX", {})
  }
  return(<header>
      <Sticky topOffset={0} isActive>{({ style }) =>         
      <Navbar bg="light" collapseOnSelect expand="lg"  className="rounded px-0 zindex-fixed shadow gradient" style={style}>
        <Container>
          <Navbar.Brand href="/" className="text-white fw-bold">Frontend Task</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse className="d-flex flex-fill" id="responsive-navbar-nav">
          <Row className="d-flex flex-fill align-items-center">
            <Col lg={6} xl={7}>
            <FormControl
              type="search"
              placeholder="🔎 Search ..."
              className="me-2 mt-2 mt-lg-0 ms-lg-2 shadow"
              aria-label="Search"
              onChange={setSearchText}
              value={searchText}
            />
            </Col>
            <Col lg={6} xl={5} className="d-flex flex-fill align-items-center">
              <Form.Select aria-label="Floating label select example" className="d-flex w-auto me-2 mt-2 mt-lg-0 mx-0 mx-lg-auto shadow" onChange={handleSelector} value={filterBy}>
                <option value="NAME">Name</option>
                <option value="AUTHOR">Author</option>
                <option value="AUTHOR_AND_NAME">Author and name</option>
                <option value="CATEGORY">Category</option>
                <option value="ALL">All</option>
              </Form.Select>
              <Form.Check type="checkbox" label="Use regex" className="d-flex w-35 align-items-center justify-content-center mt-2 mt-lg-0 flex-08 mx-auto text-white ps-0 ps-sm-2" value={+ isRegexpActive} onChange={handleRegexpCheckbox}/>
            </Col>
            </Row>
          </Navbar.Collapse>
        </Container>
      </Navbar>}
      </Sticky>
  </header>)
}
export default Header

import _ from "lodash";
import { useEffect, useMemo, useState } from "react"
import { INotConnectedArticle } from "../components/articles";
import Articles from '../dataset.json';

const DEBOUNCE_DELAY = 800

export type PreparedArticles = [INotConnectedArticle,INotConnectedArticle,INotConnectedArticle]

type PrepareDataType = (articles: INotConnectedArticle[]) => PreparedArticles[]
export const prepareData: PrepareDataType = (articles) => {
    const parts = []
    let i = 0
    const parts_length = 3
    const size = articles.length
    while(i < size){
      parts.push(articles. slice(i, i += parts_length));
    }
    return parts as PreparedArticles[]
}
type SetDatasetType =  (articles: INotConnectedArticle[]) => void

const presortDataset:(articles: INotConnectedArticle[]) => INotConnectedArticle[] = (articles) => {
  articles.sort((article_1: INotConnectedArticle, article_2: INotConnectedArticle) => {
      // @ts-ignore comment 
      return isFinite(article_1.name[0]) - isFinite(article_2.name[0]) || article_1.name.localeCompare(article_2.name, undefined, { numeric: true, sensitivity: 'base' });
  })
  return articles
}

export const useDataset:() => [PreparedArticles[],SetDatasetType] = () => {
    const [dataset, setDataset_] = useState<PreparedArticles[]>(prepareData(presortDataset(Articles.articles)))
    const setDataset:SetDatasetType = (articles=Articles.articles) => {
        const newData = prepareData(articles)
        setDataset_(newData)
    }
    return [dataset, setDataset]
}


export const useSearch:(callback: (query: string) => void) => [string, (event: any) => void] = (callback) => {
    const [searchText, setSearchText_] = useState<string>("")
    const debouncedSearch = useMemo(
      () => _.debounce((search: string) => callback(search), DEBOUNCE_DELAY),
      []
    )
    useEffect(() => {
      return () => {
        debouncedSearch.cancel();
      };
    }, [debouncedSearch]);

    const handleSearch = (event: any) => {
      const input = event.target.value;
      setSearchText_(input)
      debouncedSearch(input);
    }
    return [searchText, handleSearch]
}
import { createContext } from 'react';
import Articles from '../dataset.json'
export type EventType = 'SET_QUERY' | 'SET_FILTER' | 'SET_ACTIVE_ARTICLES' | 'INC_LOADING' | 'DEC_LOADING' | 'TOGGLE_REGEX'
export type FilterByType = "NAME" | "AUTHOR" | "CATEGORY" | "AUTHOR_AND_NAME" | "ALL"


export type DispatchSearchEventType = (actionType: EventType, payload: {query?: string, filterBy?: FilterByType, activeArticles?: number}) => void
export interface ISearchContext {
    activeArticles: number
    query: string
    filterBy: FilterByType
    isLoading: boolean
    isRegexpActive: boolean
    dispatchSearchEvent: DispatchSearchEventType
}
export const SearchContext = createContext<ISearchContext>({
    activeArticles: Articles.articles.length,
    query: "",
    filterBy: "NAME",
    isLoading: false,
    isRegexpActive: false,
    dispatchSearchEvent: () => {}
});
import json
from os import remove
import re

import sys
from enum import Enum
from typing import Iterator, Optional, List


class DataType(Enum):
    AUTHOR = "author"
    CATEGORY = "category"
    CATEGORY_ID = "categoryId"
    INDEX_ID = "indexId"

    @staticmethod
    def from_str(str_:str):
        if str_ ==  "author":
            return DataType.AUTHOR
        elif str_ == "category":
            return DataType.CATEGORY
        elif str_ == "categoryId":
            return DataType.CATEGORY_ID
        elif str_ == "indexId":
            return DataType.INDEX_ID
        raise ValueError("Unexpected string for conversion to DataType enum")

class Params:
    data_type: DataType
    text: Optional[str]

    def __init__(self, data_type: str, text: Optional[str]) -> None:
        self.data_type = DataType.from_str(data_type)
        self.text = text

    def __str__(self) -> str:
        return f'{{ "data_type": {self.data_type}, "text": {self.text} }}'

class Details:
    summary: str
    var: List[Params]

    def __init__(self, summary: str, var: List[Params]) -> None:
        self.summary = summary
        self.var = var

    def __str__(self) -> str:
        return f'{{ "summary": {self.summary}, "var": {[str(one_var) for one_var in self.var]}}}'

class Img:
    src: str
    alt: str
    width: int
    height: int

    def __init__(self, src: str, alt: str, width: int, height: int) -> None:
        self.src = src
        self.alt = alt
        self.width = width
        self.height = height

    def __str__(self) -> str:
        return f'{{ "src": {self.src}, "alt": {self.alt}, "width": {self.width}, "height": {self.height} }}'

class Figure:
    img: Img
    figcaption: str

    def __init__(self, img: Img, figcaption: str) -> None:
        self.img = img
        self.figcaption = figcaption

    def __str__(self) -> str:
        return f'{{ "img": {self.img}, "figcaption": {self.figcaption} }}'

class Article:
    figure: Figure
    details: Details

    def __init__(self, figure: Figure, details: Details) -> None:
        self.figure = figure
        self.details = details
    def __str__(self) -> str:
        return f'{{ "figure": {self.figure}, "details": {self.details} }}'

class Body:
    article: List[Article]

    def __init__(self, article: List[Article]) -> None:
        self.article = article


class Meta:
    charset: str

    def __init__(self, charset: str) -> None:
        self.charset = charset


class Head:
    meta: Meta
    title: str

    def __init__(self, meta: Meta, title: str) -> None:
        self.meta = meta
        self.title = title


class HTML:
    head: Head
    body: Body

    def __init__(self, head: Head, body: Body) -> None:
        self.head = head
        self.body = body


class Welcome8:
    html: HTML

    def __init__(self, html: HTML) -> None:
        self.html = html

class CustomImg:
    source: str
    caption: str

    def __init__(self, source: str, caption: str) -> None:
        self.source = source
        self.caption = caption

    def __str__(self) -> str:
        return { "source": self.source, "caption": self.caption }

    def toJSON(self):
        return self.__str__()

class BaseArticle:
    id: int
    name: str
    img: CustomImg
    def __init__(self, id: int, name: str, img: CustomImg) -> None:
        self.id = id
        self.img = img
        self.name = name

class NotIndexedArticle(BaseArticle):
    author: str
    categories: list[str]

    def __init__(self, id: int, name: str, author: str, categories: list[str], img: CustomImg) -> None:
        super().__init__(id, name, img)
        self.author = author
        self.categories = categories
    
    def __str__(self) -> str:
        return { "id": self.id, "name": self.name, "author": self.author, "category": self.categories, "img": self.img }
    
    def toJSON(self):
        return self.__str__()

class IndexedArticle(BaseArticle):
    author: int
    categories: list[int]

    def __init__(self, id: int, name: str, author: str, categories: list[str], img: CustomImg) -> None:
        super().__init__(id, name, img)
        self.author = author
        self.categories = categories
    
    def __str__(self) -> str:
        return { "id": self.id, "name": self.name, "author": self.author, "categories": self.categories, "img": self.img.__str__() }
    
    def toJSON(self):
        return self.__str__()


def extract_details(details: dict[str, any]) -> Details:
    """Creates from details dictionary Details object

    Args:
        details (dict[str, any]): Details dictionary

    Returns:
        Details: Details object
    """
    details_summary: str = details["summary"]
    details_vars = []
    for detail in details["var"]:
        details_vars.append(Params(data_type=detail["@data-type"], text=detail.get("#text", None)))
    return Details(details_summary, details_vars)


def extract(path="data.json") -> Iterator[Article]:
    """Extracts Articles from data file defined by path arg

    Args:
        path (str, optional): [description]. Defaults to "data.json". Path to data file

    Yields:
        Iterator[Article]: Found Article
    """
    with open(path) as file:
        data = json.load(file)
    for article in data["html"]["body"]["article"]:
        figure_dict: dict = article["figure"]
        img: dict = figure_dict["img"]
        figure = Figure(Img(src=img["@src"], alt=img["@alt"], width=img["@width"], height=img["@height"]), figure_dict["figcaption"])
        details = extract_details(article["details"])

        yield Article(figure, details)

def store_relation(value:str, *, previous_id: int=-1, ids: dict[str, int]={}, values: list[str]=[]) -> int:
    """Stores relation between objects

    Args:
        value (str): New Value to retrieve id from
        previous_id (int, optional): Previous ID value. Defaults to -1.
        ids (dict[str, int], optional): Dictionary of ids (stored by values like the one from arg). Defaults to {}.
        values (list[str], optional): List of all values (ID==INDEX_TO_VALUE_IN_LIST). Defaults to [].

    Returns:
        int: [description]
    """
    if value is None:
        return previous_id
    if value not in ids:
        new_id = previous_id + 1
        ids[value] = new_id
        values.append(value)
        return new_id
    else:
        return previous_id
def process_multiple_vals_inside_string(category: str, delimeters: list[str]=[",","&"]) -> list[str]:
    re_delimeters = "|".join(delimeters)
    return [item.lstrip() for item in re.split(re_delimeters, category)]
         
def prepare_data(category_ids: dict[str, int], categories: list[str], author_ids: dict[str, int], authors: list[str], names: list[str]) -> Iterator[NotIndexedArticle]:
    """Preloads given structures and creates NotIndexedArticle objects

    Args:
        category_ids (dict[str, int]): IDs of categories stored by category name
        categories (list[str]): Category names stored in list 
        author_ids (dict[str, int]): IDs of authors stored by author name
        authors (list[str]): Author names stored in list 
        names (list[str]): Names of all articles

    Yields:
        Iterator[NotIndexedArticle]: Prepared article
    """
    categories_id = 0
    authors_id = 0

    for index, content in enumerate(extract(), start=1):
        img_caption = content.figure.figcaption
        img_url = content.figure.img.src
        name = content.details.summary
        names.append(name)
        author = ""
        categories_local = []

        book_data = content.details.var
        for item in book_data:
            if item.data_type == DataType.AUTHOR:
                authors_id = store_relation(item.text, previous_id=authors_id, ids=author_ids, values=authors)
                author = item.text
            elif item.data_type == DataType.CATEGORY:
                for category_name in process_multiple_vals_inside_string(item.text):
                    if category_name == "":
                        continue
                    categories_id = store_relation(category_name, previous_id=categories_id, ids=category_ids, values=categories)
                    categories_local.append(category_name)
        yield NotIndexedArticle(id=index, name=name, author=author, categories=categories_local, img=CustomImg(img_url, img_caption))

def index(article: NotIndexedArticle, *, name_indexes: dict[str, int]={}, author_indexes: dict[str, set[int]]={}, category_indexes: dict[str, set[int]]={}):
    """Indexes articles and store them into given indexes dictionaries
    Index is for mapping the (shared) value on one or more ids (NotIndexedArticle) 
    Args:
        article (NotIndexedArticle): Article to index
        name_indexes (dict[str, int], optional): Indexed names. Defaults to {}.
        author_indexes (dict[str, list[int]], optional): Indexed author names. Defaults to {}.
        category_indexes (dict[str, list[int]], optional): Indexed category names. Defaults to {}.
    """
    name_indexes[article.name] = article.id
    
    #NOT unique author
    if article.author != None:
        if article.author not in author_indexes:
            author_indexes[article.author] = set()
        author_indexes[article.author].add(article.id)
    
    #NOT unique name
    for category in article.categories:
        if category != None:
            if category not in category_indexes:
                category_indexes[category] = set()
            category_indexes[category].add(article.id)

    #unique name
    if article.name != None:
        if article.name not in name_indexes:
            name_indexes[article.name] = article.id

def json_serializer(data: any):
    if isinstance(data, IndexedArticle):
        return data.__str__()
    elif isinstance(data, set):
        return list(data)
def run():
    """
    Running whole code
    """
    name_indexes = {}
    author_indexes = {}
    category_indexes = {}

    category_ids = {}
    categories = []
    
    author_ids = {}
    authors = []

    names = []

    articles = []
    
    for article in prepare_data(category_ids=category_ids, categories=categories, author_ids=author_ids, authors=authors, names=names):
        index(article, name_indexes=name_indexes, author_indexes=author_indexes, category_indexes=category_indexes)
        indexed_article = IndexedArticle(id=article.id, name=article.name, author=author_ids.get(article.author, None),categories=[category_ids.get(category_name, None) for category_name in  article.categories], img=article.img)
        articles.append(indexed_article)
    
    output_data = {
        "author_indexes": author_indexes,
        "category_indexes": category_indexes,
        "category_ids": category_ids,
        "categories": categories,
        "author_ids": author_ids,
        "authors": authors,
        "articles": articles
    }
    try:
        file_name = sys.argv[1]
    except:
        raise ValueError("Expected filename as first script param")
    with open(file_name, 'w', encoding='utf-8') as file:
       file.write(str(json.dumps(output_data, indent=4, default=json_serializer)))
run()
# Frontend Task
## Run with Docker
```console
user@linux:<inside_rontend_task>$ docker-compose up
```
or 
```console
user@linux:<inside_rontend_task>$ ./run_frontend_task.sh
```
## Run locally
```console
user@linux:<inside_rontend_task>$ docker-compose up
```
or 
```console
user@linux:<inside_rontend_task>$ npm i && npm run-script start
```

After starting the project, the website should be available at http://localhost:3000

## Tasks CSS
* [✓] The webpage has 3 parts – the header, footer and main content. 
    * `./src/App.tsx` (\<main/>)
    * `/src/components/footer.tsx` (\<footer/>)
    * `/src/components/header.tsx` (\<header/>)

* [✓] The header contains a search bar, combo box with “Name”, “Author”, “Author and name”,“Category” and “All” items, checkbox with label “Use regex” and button with text “Search”
    * `/src/components/header.tsx`

* [✓] Main content contains a list of books that are included in index.html.

    * `/src/components/articles.tsx`

* [✓] Main content contains a list of books that are included in index.html. 
    * `/src/components/articles.tsx`

* [✓] First, only the books' names are displayed. Upon hovering over a book name (bonus: upon
clicking on a book name), the name is displayed over all the three columns. Below the
book name, there are other information about the book shown. On the left side, there is a
text (author, category) and on the right side, there is an image of the book. Other books
are shown above and below the selected book.
    * `/src/components/articles.tsx`
    * `index.css`

* [✓] The header and footer have the same (appropriate) height and they are visible the whole
time during the page scrolling.
    * `/src/components/header.tsx`
    * `/src/components/footer.tsx`
    * `index.css`

* [✓] The footer contains the text “Number of found items X/Y” where X is the count of the
currently showed (which means found) books and Y is the total number of the books. If
you are not solving JS task then X and Y are the same.
    * `/src/components/header.tsx`

## Tasks JS

* [✓] The application works as a filter for the provided list of books. When the page is opened or
refreshed (and also when a search is performed with an empty search query), all the books
should be shown in an aplhabetical order.
    * `/src/components/articles.tsx`
* [✓] When the search is submitted, only books that contain the searched string in the book
name / author / category (dependeing on the selection in the combo box) will be
displayed. When “All” is selected, book should be displayed if any of its attributes matches
the search query.
    * `/src/components/articles.tsx`
    * `/src/hooks/articles.tsx`
    * `/src/hooks/services/articles.tsx`
* [✓] Search should be case insensitive.
    * `/src/hooks/services/articles.tsx`
* [✓] Update the count of the books shown in the footer to match the number of currently
displayed books.
    * `/src/hooks/context/articles.tsx`
* [✓] Describe how to install the requirements and how to start your app in the README file.
* [✓] Search results prioritize (show first) books that start with the search string.
    * `/src/hooks/services/articles.tsx`
* [✓] Implement search with regex (e.g. some of the characters can be replace with symbols like
\* . ? or other regex patterns). Search by regex is used only if the user checks the regex
checkbox in the header.
    * `/src/components/articles.ts`
* [✓] Think of one other improvement of the app and implement it. The improvement can be
minor, but it should enhance user experience or give some new interesting option that the
original app doesn’t have. If you have multiple good ideas, don’t overdo it – choose at
most two of them to implement. Don’t forget to describe your improvement in the
README.


## Improvement
 * Indexing content of every article (author and categories) => reverse tables for category ids
    *   [categoryName] : [id1, id2, id3]
    *   Assume `n` as a number of categories and `m` number of books:
        * each book can have at most `n` categories
    * Complexity (array of books and indexes)
        * array = look into all books and compare all its categories, leads to O(`m`)*O(`n`)
        * indexes = get only wanted categories and get articles connected to it, leads O(`n`) => much faster for larger dataset of books
 * Better use `ElasticSearch` for these type of tasks (not implemented)

## Structure
**docker_compose.yml** - `Docker-compose` file for starting up frontend service

**run_frontend_task.sh** - Script for running project

**./src/components/articles.tsx** - Renders articles based on search query and other parameters

**./src/components/footer.tsx** - Footer for visualising number of found articles

**./src/components/header.tsx** - Header that contains searchbar with filter selector and regex switch

**./src/components/loader.tsx** - Loader for showing progress when loading data

**./src/components/placeholder.tsx** - Placeholder used when searching for articles does not return any article (shows "No results")

**./src/hooks/** - Hooks for retrieving dataset, searchText etc.

**./services/article.ts/** - Services providing functions for retrieving and filtering articles from `dataset.json` file
```
.
├── docker-compose.yml
├── package.json
├── package-lock.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── README.md
├── run_frontend_task.sh
├── src
│   ├── App.css
│   ├── App.test.tsx
│   ├── App.tsx
│   ├── components
│   │   ├── articles.tsx
│   │   ├── footer.tsx
│   │   ├── header.tsx
│   │   ├── loader.tsx
│   │   └── placeholder.tsx
│   ├── context
│   │   └── search.ts
│   ├── dataset.json
│   ├── hooks
│   │   ├── articles.ts
│   │   └── utils.ts
│   ├── index.css
│   ├── index.tsx
│   ├── logo.svg
│   ├── react-app-env.d.ts
│   ├── reportWebVitals.ts
│   ├── services
│   │   └── articles.ts
│   ├── setupTests.ts
│   └── @types
│       └── react-lines-ellipsis
└── tsconfig.json
```
Dependencies (Docker): `docker` and `docker-compose`

Dependencies (Local): `node(16.10.0)` and `npm(8.1.0)`

**Main libraries and technologies:** `ReactJS`, `JSX`, `CSS`, `React Bootstrap` and `TypeScript`
## Preparing dataset (conversion script)
* For Converting `html` to `data.json` was used tool https://toolslick.com/conversion/data/html-to-json
* JSON `indexation` and `preparation` for JS implementation see `./data_processor/main.py` 
### Usage
```console
user@linux:<inside_frontend_task>/data_processor$ python main.py dataset.json
```
  * PARAM=outputfile

Dependencies (Docker): `docker` and `docker-compose`

Dependencies (Local): `python(3.9.7)` and `pip`
# Project structure
**docker** - Contains all `dockerfiles` for both tasks

**frontend_task** - Contains frontend task based on `ReactJS`

**python_web_task** - Contains python task based on `Django` and `PostgreSQL`

Note: Install and prepare your `docker` and `docker-compose` installation to run those projects.
```
.
├── docker
│   ├── frontend
│   └── website
├── frontend_task
│   ├── data_processor
│   │   ├── data.json
│   │   └── main.py
│   ├── docker-compose.yml
│   ├── package.json
│   ├── package-lock.json
│   ├── public
│   ├── README.md
│   ├── run_frontend_task.sh
│   ├── src
│   └── tsconfig.json
├── python_web_task
│   ├── cron_docker-compose.yml
│   ├── db_docker-compose.yml
│   ├── docker-compose.yml
│   ├── init_docker-compose.yml
│   ├── migrate_docker-compose.yml
│   ├── pyvenv.cfg
│   ├── README.md
│   ├── requirements.txt
│   ├── run_python_web_task.sh
│   ├── test_docker-compose.yml
│   └── website
└── README.md
```
Dependencies (Docker): `docker` and `docker-compose`

Dependencies (Local): `python(3.9.7)`, `pip`, `node(16.10.0)` and `npm(8.1.0)`
# Python Web Task
## Run with Docker
**First run:**
```console
user@linux:<inside_python_web_task>$ ./run_python_web_task.sh init
```
Other runs (also works with first comand):
```console
user@linux:<inside_python_web_task>$ ./run_python_web_task.sh
```
Run cron:
```console
user@linux:<inside_python_web_task>$ ./run_python_web_task.sh cron
```
Run tests:
```console
user@linux:<inside_python_web_task>$ ./run_python_web_task.sh test
```
## Run locally
```console
user@linux:<inside_python_web_task>/website$ python manage.py migrate --no-input && python manage.py loaddata init.yaml
```
Other runs:
```console
user@linux:<inside_python_web_task>/website$ python3 manage.py runserver 0.0.0.0:8000
```
Run cron:
```console
user@linux:<inside_python_web_task>/website$ python manage.py crontab add && cron -f
```
Run tests:
```console
user@linux:<inside_python_web_task>/website$ python manage.py test domain_manager/tests/ --pattern="*_test.py"
```

After `first run`, the website should be available at http://0.0.0.0:8000/
## Tasks
* [✓] Create a database with the objects specified above. Provide some example data, e.g. as Django fixtures. 
    * `./website/domain_manager/fixtures/init.yaml`
    * `/website/domain_manager/models.py`
* [✓] Create a Domain detail web page. It will show the attributes of the Domain and its currently active flags (take into account that the flags can be planned even to the future). 
    * `./website/domain_manager/templates/detail.html`
    * `./website/domain_manager/queries/detail.py`
    * `./website/domain_manager/views/domain_detail.py`

* [✓] Create a web page with a list of existing (not deleted) Domains. For each Domain, display
its FQDN and currently active flags. The FQDN will also serve as a link to the Domain detail page. 
    * `./website/domain_manager/templates/index.html`
    * `./website/domain_manager/queries/domain_list.py`
    * `./website/domain_manager/views/domain_list.py`

* [✓] Write tests for Domain detail page.
    * `./website/domain_manager/tests/page_test.py`
    * `./website/domain_manager/tests/models_test.py`
    * `./website/domain_manager/tests/views_test.py`

* [✓] BONUS: Create a command/script, that will be run daily by cron daemon. This command will set the EXPIRED Domain flag to all domains that have Expiration date today.
    * `./website/domain_manager/cron/domain.py`
* [✓] BONUS 2: Also write tests for the Domain list page and the command/script above. 
    * `./website/domain_manager/tests/page_test.py`
    * `./website/domain_manager/tests/models_test.py` (add_flag_to_expired_domain - cron)
    * `./website/domain_manager/tests/views_test.py`]

## Structure
**\*_docker_compose.yml** - `Docker-compose` files for starting up each service (db, server, migration tools and cron)

**run_python_web_task.sh** - Script for running wanted parts of project

**./website/domain_manager/models.py** - Objects definition and its connection to each other

**./website/domain_manager/test/** - Tests for each par of this projects (queries, rendering (Selenium) and responses)

**./website/domain_manager/templates/** - HTML templates for detail and domain list pages

**./website/domain_manager/views/** - Views (based on classes) handle requests from client and provide answers wrapped into `templates`

**./website/domain_manager/cron/** - Cron scripts

**./website/domain_manager/migrations/** - Migration scripts for changes in DB

...
```
.
├── cron_docker-compose.yml
├── db_docker-compose.yml
├── docker-compose.yml
├── init_docker-compose.yml
├── migrate_docker-compose.yml
├── README.md
├── requirements.txt
├── run_python_web_task.sh
├── test_docker-compose.yml
└── website
    ├── domain_manager
    │   ├── admin.py
    │   ├── apps.py
    │   ├── cron
    │   ├── fixtures
    │   ├── __init__.py
    │   ├── migrations
    │   ├── models.py
    │   ├── __pycache__
    │   ├── queries
    │   ├── static
    │   ├── templates
    │   ├── tests
    │   ├── urls.py
    │   └── views
    ├── manage.py
    ├── wait_for_postgres.sh
    └── website
        ├── asgi.py
        ├── __init__.py
        ├── __pycache__
        ├── settings.py
        ├── urls.py
        └── wsgi.py
```
Dependencies (Docker): `docker` and `docker-compose`

Dependencies (Local): `python(3.9.7)` and `pip`
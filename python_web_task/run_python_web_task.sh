#!/bin/bash

if [ "$1" = "init" ]; then
    shift;
    docker-compose -f db_docker-compose.yml -f docker-compose.yml -f init_docker-compose.yml up --build $@;
elif [ "$1" = "migrate" ]; then
    shift;
    docker-compose -f db_docker-compose.yml -f migrate_docker-compose.yml up --build $@;
elif [ "$1" = "test" ]; then
    shift;
    docker-compose -f db_docker-compose.yml -f test_docker-compose.yml up --build $@;
elif [ "$1" = "cron" ]; then
    shift;
    docker-compose -f db_docker-compose.yml -f cron_docker-compose.yml up --build $@;
else
    docker-compose -f db_docker-compose.yml -f docker-compose.yml up --build $@;
fi
from datetime import datetime
from django.db import models
from django.db.models.query_utils import Q

class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    
    class Meta:
        abstract = True

class DomainFlag(models.Model):
    name = models.CharField(max_length=16)
    def __str__(self) -> str:
        return f"domain_flag_name={self.name}"

class DomainFlagHistory(models.Model):
    domain_flag = models.ForeignKey(DomainFlag, on_delete=models.CASCADE)
    valid_from = models.DateTimeField(auto_now_add=True, editable=False)
    valid_to = models.DateTimeField(editable=True, null=True)

    def __str__(self) -> str:
        return f"domain_flag={self.domain_flag} valid_from={self.valid_from} valid_to={self.valid_to}"

    def get_from_to_filter(timestamp: datetime):
        return Q(Q(valid_from__lte=timestamp) and Q(Q(valid_to__gte=timestamp) | Q(valid_to__isnull=True)))

class Domain(TimeStamp):
    name = models.CharField(max_length=255)
    expiring_at = models.DateTimeField(null=True)
    deleted_at = models.DateTimeField(null=True)
    domain_flags=models.ManyToManyField(to=DomainFlagHistory)
    def __str__(self) -> str:
        return f"domain={self.name}, created_at={self.created_at}, expiring_at={self.expiring_at} domain_flags={self.domain_flags} deleted_at={self.deleted_at}"
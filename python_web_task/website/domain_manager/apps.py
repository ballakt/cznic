from django.apps import AppConfig


class DomainManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'domain_manager'

from datetime import datetime
from django.db.models.base import Model

from django.db.models.query import QuerySet

from domain_manager.models import Domain
from domain_manager.queries.shared.domain import prefetch_domain_flags


def get_domain_with_domain_flags(id: int, timestamp: datetime) -> QuerySet[Model]:
    """Retrieves domain with its domain flags

    Args:
        id (int): Domain ID
        timestamp(datetime): Timestamp

    Returns:
        QuerySet[_T]: Domain with domain flags
    """
    return prefetch_domain_flags(Domain.objects.filter(id=id), timestamp).first()

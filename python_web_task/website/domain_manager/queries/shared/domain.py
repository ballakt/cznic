from django.db.models.base import Model
from django.db.models.query import  Prefetch, QuerySet
from datetime import datetime

from django.db.models.query_utils import Q

from domain_manager.models import Domain, DomainFlagHistory

def prefetch_domain_flags(query: QuerySet[Model], timestamp: datetime) -> QuerySet[Model]:
    """Connect domain flags to given BaseManager

    Args:
        query (QuerySet[Model]): Connects domain flags to given query
        timestamp (datetime): Timestamp used by between function

    Returns:
        QuerySet[Model]: Result
    """
    return query.prefetch_related(Prefetch('domain_flags', queryset=DomainFlagHistory.objects.filter(DomainFlagHistory.get_from_to_filter(timestamp))))

def get_not_deleted_domain(domain: QuerySet[Model], timestamp: datetime) -> QuerySet[Model]:
    return domain.filter(Q(Q(deleted_at__isnull=True) | Q(deleted_at__gt=timestamp)))
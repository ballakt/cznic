from datetime import datetime
from django.db.models.base import Model

from django.db.models.query import QuerySet
from django.utils import timezone

from domain_manager.models import Domain, DomainFlag, DomainFlagHistory
from domain_manager.queries.shared.domain import get_not_deleted_domain, prefetch_domain_flags

def create_domain_flag_history(domain_flag: DomainFlag, valid_from: datetime, valid_to: datetime=None) -> QuerySet[Model]:
    """Creates DomainFlagHistory record

    Args:
        domain_flag (DomainFlag): DomainFlag
        valid_from (datetime): Date from which is flag valid
        valid_to (datetime, optional): [description]. Date when domain flag becomes invalid (if None -> will be always valid)

    Returns:
        QuerySet[Model]: [description]
    """
    return DomainFlagHistory.objects.create(domain_flag=domain_flag, valid_from=valid_from, valid_to=valid_to)

def add_flag_to_expired_domain():
    """
    Adds EXPIRED flag to domain that is considered as expired (expired_at<now)
    """
    expired = get_expired_domains()
    if expired is None:
        return
    
    expiring_flag = DomainFlag.objects.filter(name='EXPIRED').first()
    for one_expired in expired:
        exists = one_expired.domain_flags.filter(domain_flag__name='EXPIRED').exists()
        if not exists:
            one_expired.domain_flags.add(create_domain_flag_history(expiring_flag, timezone.now()))

def get_expired_domains() -> QuerySet[Model]:
    """Returns only expired domains

    Returns:
        QuerySet[Model]: Expired domains
    """
    now = timezone.now()
    not_deleted = get_not_deleted_domain(Domain.objects, now)
    expired = not_deleted.filter(expiring_at__lte=now)
    return expired

def get_domain_with_domain_flags(id: int, timestamp: datetime) -> QuerySet[Model]:
    """Retrieves domain with its domain flags

    Args:
        id (int): Domain ID
        timestamp(datetime): Timestamp

    Returns:
        QuerySet[_T]: Domain with domain flags
    """
    return prefetch_domain_flags(Domain.objects.filter(id=id), timestamp).first()

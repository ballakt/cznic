from datetime import datetime

from django.db.models.query import QuerySet

from domain_manager.models import Domain
from domain_manager.queries.shared.domain import get_not_deleted_domain, prefetch_domain_flags

def get_domain_list_with_domain_flags(timestamp: datetime) -> list[dict[str, any]]:
    """Retrieves domain with its domain flags

    Args:
        timestamp(datetime): Timestamp

    Returns:
        list[dict[str, any]]: Domain list with domain flags
    """
    domains =  prefetch_domain_flags(get_not_deleted_domain(Domain.objects, timestamp), timestamp).all()
    output = []
    for domain in domains:
        domain_flag_ids = domain.domain_flags.all().values("domain_flag__id")
        output.append({
            "id": domain.id,
            "name": domain.name,
            "domain_flags": [entry["domain_flag__id"] for entry in domain_flag_ids],
    })
    return output

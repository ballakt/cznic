from django.urls import path

from domain_manager.views.domain_list import DomainList
from domain_manager.views.domain_detail import DomainDetail

urlpatterns = [
    path('', DomainList.as_view(), name='index'),
    path('detail/<int:id>/', DomainDetail.as_view(), name='detail'),
]

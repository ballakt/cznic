import datetime
from django.views import View
from django.utils import timezone
from django.shortcuts import render
from domain_manager.queries.domain_list import get_domain_list_with_domain_flags
from domain_manager.models import DomainFlag
from domain_manager.queries.cron import add_flag_to_expired_domain


def get_index_context(timestamp: datetime) -> dict[str, any]:
    """Creates context for / route

    Args:
        timestamp (datetime): Connection of domain_flags to each
        
        domain based on timestamp (timestamp between valid_from 
        
        and valid_to datetime)

    Returns:
        dict[str, any]: Context for /
    """
    domains = get_domain_list_with_domain_flags(timestamp)
    domain_flags = DomainFlag.objects.order_by("id")

    context = {'domains': domains, 'domain_flags': domain_flags, 'domain_flags_range': range(1, len(domain_flags) + 1)}
    return context

class DomainList(View):
    template_name="index.html"
    def get(self, request):
        return render(request, self.template_name, get_index_context(timezone.now()))
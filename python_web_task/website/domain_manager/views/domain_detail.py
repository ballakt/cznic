import datetime
from django.http.response import Http404
from django.views import View
from django.utils import timezone
from django.shortcuts import render
from django.db.models.base import Model
from django.db.models.query import QuerySet
from domain_manager.queries.detail import get_domain_with_domain_flags


def get_detail_context(id: int, timestamp: datetime) -> QuerySet[Model]:
    """Creates context for /detail route

    Args:
        id (int): ID of the certain domain
        timestamp (datetime): Connection of domain_flags to each
        
        domain based on timestamp (timestamp between valid_from 
        
        and valid_to datetime)

    Returns:
        QuerySet[Model]: [description]
    """
    return get_domain_with_domain_flags(id, timestamp)

class DomainDetail(View):
    template_name="detail.html"
    
    def get(self, request, *args,**kwargs):
        domain = get_detail_context(kwargs["id"], timezone.now())
        if not domain:
            raise Http404("Domain with given ID does not exist")
        context = {"domain": get_detail_context(kwargs["id"], timezone.now())}
        return render(request, self.template_name, context)
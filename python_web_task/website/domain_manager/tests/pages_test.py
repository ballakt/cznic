import re
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

class MySeleniumTests(StaticLiveServerTestCase):
    
    fixtures = ['init.yaml']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = Options()
        options.add_argument("--headless")
        driver = webdriver.Firefox(options=options)
        cls.selenium = driver
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def test_domain_list_page(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/'))
        expired_column_header = self.selenium.find_element_by_xpath("//th[contains(text(),'EXPIRED')]")
        outzone_column_header = self.selenium.find_element_by_xpath("//th[contains(text(),'OUTZONE')]")
        delete_column_header = self.selenium.find_element_by_xpath("//th[contains(text(),'DELETE_CANDIDATE')]")

        self.assertEqual(expired_column_header.text, "EXPIRED")
        self.assertEqual(outzone_column_header.text, "OUTZONE")
        self.assertEqual(delete_column_header.text, "DELETE_CANDIDATE")

        #Testing URLs of domains (first column)
        expected_urls = ["/detail/1", "/detail/2", "/detail/3"]
        found_paths= []
        domain_columns = self.selenium.find_elements_by_xpath("//th[@class='text-start']/a")
        for domain_ahref in domain_columns:
            url = domain_ahref.get_attribute("href")
            path = re.sub("http://localhost:[0-9]+", "", url)
            found_paths.append(path)
        
        self.assertEqual(found_paths, expected_urls, msg="Unexpected URLs of domains")

        #Testing others cell
        cross = "bi bi-x"
        checkmark = "bi bi-check text-success"

        expected_classnames = [cross, checkmark, cross, cross, cross, cross, cross, checkmark, cross]
        domain_flags_cells = self.selenium.find_elements_by_xpath("//tr/td/i")

        for index, val in enumerate(domain_flags_cells):
            self.assertEqual(val.get_dom_attribute("class"), expected_classnames[index], msg=f"Unexpexted mark for cell number")

    def test_detail_domain_page(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/detail/1'))

        #Checking domain name
        main_title = self.selenium.find_element_by_xpath("//div[@class='square']/p")
        self.assertEqual(main_title.text, "nic.cz.", msg="Missing label nic.cz.(domain name) inside the large square")

        #Checking domain flags
        label = self.selenium.find_element_by_xpath("//div[@class='labels']/span")   
        self.assertEqual(label.text, "OUTZONE", msg="Missing domain flag OUTZONE inside the main square")

        #Checking domain dates
        dates = self.selenium.find_elements_by_xpath("//span[contains(@class, 'badge rounded-pill bg-light text-dark')]")
        self.assertEqual(dates[0].text, "12.10. 2021", msg="Unexpected Created at date")
        self.assertEqual(dates[1].text, "20.11. 2021", msg="Unexpected Expiring at date")
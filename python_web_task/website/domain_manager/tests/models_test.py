from datetime import timedelta
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.utils import timezone
from django.test import TransactionTestCase
from domain_manager.models import Domain, DomainFlag, DomainFlagHistory
from domain_manager.queries import detail, domain_list
from domain_manager.queries.cron import add_flag_to_expired_domain, get_expired_domains

class ModelsTest(StaticLiveServerTestCase):
    
    fixtures = ['init.yaml']

    flag_1: DomainFlag
    flag_2: DomainFlag
    flag_3: DomainFlag

    flag_history_1_outdated: DomainFlagHistory
    flag_history_2: DomainFlagHistory
    flag_history_3_future: DomainFlagHistory

    domain: Domain
    domain_deleted: Domain

    now = timezone.now()

    def setUp(self):
        self.flag_1 = DomainFlag.objects.create(name="FLAG_1")
        self.flag_2 = DomainFlag.objects.create(name="FLAG_2")
        self.flag_3 = DomainFlag.objects.create(name="FLAG_3")


    def test_domain_detail_with_flags(self):
        domain_with_flags = detail.get_domain_with_domain_flags(id=1, timestamp=self.now)
        self.assertEqual(len(domain_with_flags.domain_flags.all()), 1, msg="Exactly one Domain Flag is expected (second is outdated)")

    def test_deleted_domain_detail_with_flags(self):
        deleted_domain_with_flags = detail.get_domain_with_domain_flags(id=4, timestamp=self.now)
        self.assertIsNotNone(deleted_domain_with_flags, msg="Expected deleted domain")

    def test_domain_list_with_flags(self):
        domains_with_flags = domain_list.get_domain_list_with_domain_flags(timestamp=self.now)
        self.assertEqual(len(domains_with_flags), 3, msg="Exactly four Domains are expected (one is deleted)")
    
    def test_expired_domain(self):
        self.assertFalse(get_expired_domains().filter(id=3).first().domain_flags.filter(domain_flag__name='EXPIRED').exists(), msg="Domains should not have the EXPIRED flag")
        add_flag_to_expired_domain()
        self.assertTrue(get_expired_domains().filter(id=3).first().domain_flags.filter(domain_flag__name='EXPIRED').exists(), msg="Domains should have the EXPIRED flag")

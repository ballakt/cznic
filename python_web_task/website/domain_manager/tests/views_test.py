from unittest.case import TestCase
from django.test.client import Client

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_list(self):
        request = self.client.get('/')
        self.assertEqual(request.status_code, 200)